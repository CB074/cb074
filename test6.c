#include <stdio.h>
#include <stdlib.h>

int main()
{
    int maxmarks;
   int marks[5][3];
   for(int i=0;i<5;i++)
   {
        printf("\n Enter marks of student %d:",i+1);
       for(int j=0;j<3;j++)
       {
          printf("\n marks[%d][%d] = ",i,j);
          scanf("%d",&marks[i][j]);
       }
   }

       for(int j=0;j<3;j++)
       {
           for(int i=0;i<5;i++)
           {
               if(maxmarks<marks[i][j])
               {
                   maxmarks=marks[i][j];
               }
           }
               printf("\nThe highest marks in subject %d = %d",j+1,maxmarks);
        }

           return 0;
}
