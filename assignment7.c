#include <stdio.h>
#include <stdlib.h>

int main()
{
    int upperdiagsum=0;
    int lowerdiagsum=0;
    int diagsum=0;
    int n;
    int arr[100][100];
    printf("\n please enter the order of your matrix:");
    scanf("%d",&n);
   for(int i=0;i<n;i++)
   {
       for(int j=0;j<n;j++)
       {
           printf("\n Please enter the element arr[%d][%d]:",i,j);
           scanf("%d",&arr[i][j]);
       }
   }
  printf("\n This is the matrix:");
  for(int i=0;i<n;i++)
  {
      printf("\n");
      for(int j=0;j<n;j++)
      {
          printf("%d",arr[i][j]);
      }
  }
  printf("\n The sum of all the diagonal elements is:");
  for(int i=0;i<n;i++)
  {
      for(int j=0;j<n;j++)
    {
        if(i==j)
        {
            diagsum=diagsum+arr[i][j];
        }
        if(j>i)
        {
            upperdiagsum=upperdiagsum+arr[i][j];
        }
        if(i>j)
        {
            lowerdiagsum=lowerdiagsum+arr[i][j];
        }
    }
  }
   printf("%d",diagsum);
   printf("\n The sum of elements above the diagonal is:");
   printf("%d",upperdiagsum);
   printf("\n The sum of elements below the diagonal is:");
   printf("%d",lowerdiagsum);

return 0;
}
