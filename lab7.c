#include <stdio.h>
int main()
{
  char input[100];
    int li, ri, length = 0;
    printf("Enter a string for palindrome check\n");
    scanf("%s", input);
    while(input[length]!='\0')
        length++;
    if(length<1)
        return 1;
    li=0;
    ri=length -1;
    while(li < ri)
        {
        if(input[li] != input[ri])
        {
            printf("%s is not a Palindrome \n", input);
            return 0;
        }
        li++;
        ri--;
    }
    printf("%s is a Palindrome \n", input);
return 0;
}

