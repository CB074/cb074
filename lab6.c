#include <stdio.h>
#include <stdlib.h>

int main()
{
 int a[10][10], transpose[10][10], x, y;
 printf("Enter rows and columns: ");
 scanf("%d %d", &x, &y);
 printf("\nEnter elements of the matrix:\n");
 for (int i = 0; i < x; ++i)
        for (int j = 0; j < y; ++j)
            {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &a[i][j]);
            }
printf("\nDisplaying Entered matrix:\n");
for (int i = 0; i < x; ++i)
        for (int j = 0; j < y; ++j)
            {
            printf("%d  ", a[i][j]);
            if (j==y-1)
                printf("\n");
            }
for (int i=0;i<x;++i)
        for (int j=0;j<y;++j)
            {
            transpose[j][i] = a[i][j];
            }
printf("\nTranspose of the matrix:\n");
    for (int i=0;i<x;++i)
        for (int j=0;j<y;++j)
            {
            printf("%d  ", transpose[i][j]);
            if (j==y-1)
                printf("\n");
            }
    return 0;
}